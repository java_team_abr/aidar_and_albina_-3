package krestiki_noliki.Connection;

public interface Connection {
    public void sendMessage(String message);
    public String getMessage();
}
