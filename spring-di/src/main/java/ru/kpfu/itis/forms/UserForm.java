

package ru.kpfu.itis.forms;

import lombok.Getter;

@Getter
public class UserForm {
    private String login;

    private String password;

    private String email;

    private String name;
}