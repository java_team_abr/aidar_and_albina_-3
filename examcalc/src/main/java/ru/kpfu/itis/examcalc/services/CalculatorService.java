package ru.kpfu.itis.examcalc.services;

import org.springframework.stereotype.Service;
import ru.kpfu.itis.examcalc.dto.CalculatorForm;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@Service
public class CalculatorService {
    public float calculateService(CalculatorForm calculatorForm) {
        float first = Float.parseFloat(calculatorForm.getFirst());
        float second = Float.parseFloat(calculatorForm.getSecond());
        switch (calculatorForm.getOperation()) {
            case "+":
                return first + second;
            case "-":
                return first - second;
            case "*":
                return first * second;
            default:
                return first / second;
        }
    }
}
