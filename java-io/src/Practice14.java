import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/*
    Составить статистику, сколько раз буква упоминается в файле с английским текстом
*/

public class Practice14 {
    public static void main(String[] args) {
        String fileName = "src/english-text.txt";
        Map<Character, Integer> statistics = getStatistics(fileName);
        System.out.println(Collections.singletonList(statistics));
    }

    public static Map<Character, Integer> getStatistics(String filePath) {
        Map<Character, Integer> statistics = new HashMap<>();
        int current = 'a';
        int z = 'z';
        while(current<=z){
            statistics.put(current,0);
            current++;
        }


        statistics.put('a', 0);
        statistics.put('b', 0);
        statistics.put('c', 0);
        statistics.put('d', 0);
        statistics.put('e', 0);
        statistics.put('f', 0);
        statistics.put('g', 0);
        statistics.put('h', 0);
        statistics.put('i', 0);
        statistics.put('j', 0);
        statistics.put('k', 0);
        statistics.put('l', 0);
        statistics.put('m', 0);
        statistics.put('n', 0);
        statistics.put('o', 0);
        statistics.put('p', 0);
        statistics.put('q', 0);
        statistics.put('r', 0);
        statistics.put('s', 0);
        statistics.put('t', 0);
        statistics.put('u', 0);
        statistics.put('v', 0);
        statistics.put('w', 0);
        statistics.put('x', 0);
        statistics.put('y', 0);
        statistics.put('z', 0);

        List<Character> text = new ArrayList<>();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(new File(filePath)));
            int c;

            while ((c = reader.read()) != -1) {
                text.add((char) c);
            }

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Character character : text) {
            if (statistics.containsKey(character)) {
                statistics.computeIfPresent(character, (k, v) -> v + 1);
            }
        }

        return statistics;
    }
}