package krestiki_noliki.Connection;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerSocketConnection implements Connection{
    private Socket socket;
    private final int port;
    private Writer writer;
    private Reader reader;

    public static class ServerSocketConnectionSingleton{
        private ServerSocketConnection serverSocketConnection;
        public ServerSocketConnection getConnection(int p){
            if(serverSocketConnection == null) {
                serverSocketConnection = new ServerSocketConnection(p);
            }
            return serverSocketConnection;
        }
    }

    private ServerSocketConnection(int port) {
        this.port = port;
        try {
            this.socket = new ServerSocket(port).accept();
            this.writer = new OutputStreamWriter(socket.getOutputStream());
            this.reader = new InputStreamReader(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendMessage(String message) {
        try {
            writer.write(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getMessage() {
        String received = null;
        while (received == null){
            try {
                received = String.valueOf(reader.read());
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                this.wait(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return received;
    }
}
