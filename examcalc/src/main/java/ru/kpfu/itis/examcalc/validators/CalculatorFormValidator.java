package ru.kpfu.itis.examcalc.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.kpfu.itis.examcalc.dto.CalculatorForm;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@Component
public class CalculatorFormValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return CalculatorForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        CalculatorForm form = (CalculatorForm) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "first", "empty.first", "Пустой первый аргумент");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "second", "empty.second", "Пустой второй аргумент");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "operation", "empty.operation", "Пустая операция");
        try {
            Float.parseFloat(form.getFirst());
        } catch (Exception e) {
            errors.reject("bad.first", "Первый аргумент не число");
        }
        try {
            Float.parseFloat(form.getSecond());
        } catch (Exception e) {
            errors.reject("bad.second", "Второй аргумент не число");
        }
        if (!(form.getOperation().equals("+") || form.getOperation().equals("-") ||
                form.getOperation().equals("*") || form.getOperation().equals("/"))) {
            errors.reject("bad.operation", "Невалидная оперция");
        }
        if (form.getOperation().equals("/") && Float.parseFloat(form.getSecond()) == 0.0) {
            errors.reject("zero.devision", "Деление на 0 запрещено");
        }
    }
}
