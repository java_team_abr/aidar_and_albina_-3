package main.java.ru.kpfu.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.kpfu.itis.forms.UserForm;
import ru.kpfu.itis.models.User;
import ru.kpfu.itis.repositories.UserRepository;

public class UserServiceImpl implements ru.kpfu.itis.services.UserService {
    private final UserRepository userRepository;

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void register(UserForm userForm) {
        User user = User.builder()
                .login(userForm.getLogin())
                .password(passwordEncoder.encode(userForm.getPassword()))
                .email(userForm.getEmail())
                .name(userForm.getName())
                .build();

        userRepository.save(user);
    }
}
