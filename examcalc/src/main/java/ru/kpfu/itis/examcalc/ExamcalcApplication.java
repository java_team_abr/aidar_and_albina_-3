package ru.kpfu.itis.examcalc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class ExamcalcApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExamcalcApplication.class, args);
    }
}
