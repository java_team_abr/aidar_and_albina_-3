package krestiki_noliki.Connection;

import java.io.*;
import java.net.Socket;

public class SocketConnection implements Connection{
    private Reader reader;
    private Writer writer;
    private Socket socket;
    private int port;
    private String ip;

    private SocketConnection(String ip, int port) {
        try {
            this.socket = new Socket(ip,port);
            reader = new InputStreamReader(socket.getInputStream());
            writer = new OutputStreamWriter(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class SocketConnectionSingleton {
        SocketConnection socketConnection;
        public SocketConnection getSocketConnection(int port,String ip ){
            if (socketConnection == null){
                socketConnection = new SocketConnection(ip, port);
            }
            return socketConnection;
        }
    }

    @Override
    public void sendMessage(String message) {
        try {
            writer.write(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getMessage() {
        String message = null;
        while (message == null){
            try {
                message = String.valueOf(reader.read());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return message;
    }
}
