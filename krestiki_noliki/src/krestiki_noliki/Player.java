package krestiki_noliki;

import krestiki_noliki.Connection.Connection;
import krestiki_noliki.Connection.ServerSocketConnection;
import krestiki_noliki.Connection.SocketConnection;

public class Player {
    Connection connection;
    Role role;

    public Player(Connection connection, Role role) {
        this.connection = connection;
    }

    public Player createServer(int port) {
        connection = new ServerSocketConnection.ServerSocketConnectionSingleton().getConnection(port);
        return new Player(connection, role);
    }

    public Player createClient(String ip, int port) {
        connection = new SocketConnection.SocketConnectionSingleton().getSocketConnection(port, ip);
        return new Player(connection, role);
    }

    public void setRole(Role role){
        this.role = role;
    }

}
