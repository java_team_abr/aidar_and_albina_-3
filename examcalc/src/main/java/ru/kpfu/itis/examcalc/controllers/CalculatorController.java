package ru.kpfu.itis.examcalc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.kpfu.itis.examcalc.dto.CalculatorForm;
import ru.kpfu.itis.examcalc.services.CalculatorService;
import ru.kpfu.itis.examcalc.validators.CalculatorFormValidator;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Arrays;
import java.util.StringJoiner;
import java.util.stream.Collectors;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@Controller
@RequestMapping("/hi")
public class CalculatorController {
    private final CalculatorFormValidator calculatorFormValidator;
    private final CalculatorService calculatorService;

    @Autowired
    public CalculatorController(CalculatorFormValidator calculatorFormValidator, CalculatorService calculatorService) {
        this.calculatorFormValidator = calculatorFormValidator;
        this.calculatorService = calculatorService;
    }

    @InitBinder("form")
    public void initAdaptedQuestionFormValidator(WebDataBinder binder) {
        binder.addValidators(calculatorFormValidator);
    }

    @GetMapping
    public String calculator() {
        return "calc";
    }

    @PostMapping
    public String calc(@Valid @ModelAttribute("form") CalculatorForm form,
                       BindingResult errors,
                       RedirectAttributes redirectAttributes) {
        if (errors.hasErrors()) {
            redirectAttributes.addFlashAttribute("error", errors.getAllErrors()
                    .stream()
                    .map(ObjectError::getDefaultMessage)
                    .collect(Collectors.joining(". ")));
        } else {
            redirectAttributes.addFlashAttribute("result", calculatorService.calculateService(form));
        }
        redirectAttributes.addFlashAttribute("form", form);
        return "redirect:/hi";
    }
}
