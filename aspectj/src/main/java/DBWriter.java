import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DBWriter {

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        String driverClass = "org.postgresql.Driver";
        String url = "jdbc:postgresql://localhost:5432/postgres";
        Class.forName(driverClass);
        Connection connection = DriverManager.getConnection(url,"postgres","postgres");

        Statement st = connection.createStatement();
        st.execute("INSERT into new_database.new_schema.my_user" +
                "(name) values " +
                "('injection')");



    }

}
