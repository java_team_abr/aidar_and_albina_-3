import java.util.*;
import java.util.Map.Entry;

/*
    Отсортировать коллекцию по возрастанию количества неповторяющихся чисел
    (123 - 3 неповторяющихся, 122 - 1 неповторяющееся)

    Скорее всего можно и проще переписать
*/

public class Task2 {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(121212);
        numbers.add(122);
        numbers.add(123);

        List<Entry<Integer, Integer>> sort = sort(numbers);

        System.out.println(Collections.singletonList(sort));
    }

    public static List<Entry<Integer, Integer>> sort(List<Integer> numbers) {
        // Map, куда мы засовываем встречающиеся в числе цифры (после каждого прохода обнуляем)
        Map<Character, Integer> digits = new HashMap<>();
        // Map, куда мы засовываем число и количество неповторяющихся цифр
        Map<Integer, Integer> result = new HashMap<>();

        for (Integer number : numbers) {
            char[] array = number.toString().toCharArray();

            for (char a : array) {
                if (!digits.containsKey(a)) {
                    digits.put(a, 1);
                } else {
                    digits.computeIfPresent(a, (k, v) -> v + 1);
                }
            }

            int sum = 0;

            Object[] keySet = digits.keySet().toArray();

            for (Object key : keySet) {
                if (digits.get(key) == 1) {
                    sum++;
                }
            }

            result.put(number, sum);

            digits.clear();
        }

        List<Entry<Integer, Integer>> list = new ArrayList<>(result.entrySet());
        list.sort(Comparator.comparingInt(Entry::getValue));

        return list;
    }
}