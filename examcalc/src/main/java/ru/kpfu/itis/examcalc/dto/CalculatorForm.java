package ru.kpfu.itis.examcalc.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Rustem Saitgareev
 * 11-602
 * 000
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CalculatorForm {
    private String first;
    private String second;
    private String operation;
}
