import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/*
    Вывести HashMap, содержащий количество повторений чисел в файле input.txt
*/

public class Task3 {
    public static void main(String[] args) {
        String fileName = "src/input.txt";
        Map<Integer, Integer> repetitions = findRepetitions(fileName);
        System.out.println(Collections.singletonList(repetitions));
    }

    public static Map<Integer, Integer> findRepetitions(String filePath) {
        Map<Integer, Integer> repetitions = new HashMap<>();

        try {
            Path path = Paths.get(filePath);

            int[] numbers = Files.lines(path)
                    .flatMap(e -> Stream.of(e.split(" ")))
                    .mapToInt(Integer::parseInt)
                    .toArray();

            for (int number : numbers) {
                if (!repetitions.containsKey(number)) {
                    repetitions.put(number, 1);
                } else {
                    repetitions.computeIfPresent(number, (k, v) -> v + 1);
                }
            }
        } catch (IOException | NumberFormatException e) {
            e.printStackTrace();
        }

        return repetitions;
    }
}