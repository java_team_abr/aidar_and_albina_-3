import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/*
    Отсортировать строки в файле по заданной букве (допустим, в строке по одному слову)
    Сортируем по третьей букве по алфавиту, например
    Регистр учитывается!!!
*/

public class Task1 {
    public static void main(String[] args) {
        String fileName = "src/sort.txt";
        List<String> sorted = sort(fileName, 3);
        System.out.println(Collections.singletonList(sorted));
    }

    public static List<String> sort(String filePath, int letterNumber) {
        try {
            Object[] lines = Files.lines(Paths.get(filePath), StandardCharsets.UTF_8).toArray();

            List<Object> objects = Arrays.asList(lines);

            return objects.stream()
                    .map(object -> Objects.toString(object, null)).sorted((o1, o2) -> {
                        int p1 = Math.min(o1.length() - 1, letterNumber - 1);
                        int p2 = Math.min(o2.length() - 1, letterNumber - 1);

                        return o1.charAt(p1) - o2.charAt(p2);
                    }).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}