package ru.kpfu.itis.services;

import org.springframework.stereotype.Service;
import ru.kpfu.itis.forms.UserForm;

@Service
public interface UserService {
    void register(UserForm userForm);
}