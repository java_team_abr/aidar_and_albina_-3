import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;


aspect CheckInjection {

   /* pointcut checkSQLInjection(String s): call(* java.sql.*Statement.execute*(String query));

    before(String s) : checkSQLInjection(String s){
        System.out.println(
                "------------------- Aspect Logic --------------------");


        System.out.println("Captured String parameter on method: " + s);
        if (s.contains("\";")){
            try {
                throw new Exception("Injection found");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            System.out.println("No injection");
        }

    }*/

    @Around("* java.sql.*Statement.execute*(String query)")
    public Object handle(ProceedingJoinPoint jp) throws Throwable {
        String query = (String) jp.getArgs()[0];
        if (query.startsWith("\";")||query.toLowerCase().contains("injection")) {
            System.out.println("Injection detected");
            return null;
        }
        return jp.proceed();
    }


/*    @Around("checkSQLInjection()")
    public Object handleExecution(ProceedingJoinPoint p) throws Throwable {
        String methodName = p.getSignature().getName();
        Object[] methodArgs = p.getArgs();
        String query = (String) methodArgs[0];
        System.out.println("Checking for injections");
        if (query.contains("\";")){
            System.out.println("Injection found");
        }
        else {
            System.out.println("No injections");
            Object result = p.proceed();
            System.out.println("Execution successfull");
            return result;
        }
        throw new RuntimeException();
    }*/
}
