<#if error??>
    <p>Ошибка: ${error}</p>
</#if>
<form method="post">
    <input name="first" type="text" <#if form?? && form.first??>value="${form.first}"</#if> />
    <select name="operation">
        <option <#if form?? && form.operation?? && form.operation == "+">selected</#if>>+</option>
        <option <#if form?? && form.operation?? && form.operation == "-">selected</#if>>-</option>
        <option <#if form?? && form.operation?? && form.operation == "*">selected</#if>>*</option>
        <option <#if form?? && form.operation?? && form.operation == "/">selected</#if>>/</option>
    </select>
    <input name="second" type="text" <#if form?? && form.second??>value="${form.second}"</#if> />
    <input type="submit" value="Send" />
</form>
<#if result??>
    <p>Результат: ${result}</p>
</#if>