import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
    Отсортировать коллекцию чисел по возрастанию чисел,
    полученных из исходных путём отображения чисел в обратном порядке
*/

public class Practice12 {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(123);
        numbers.add(51);
        numbers.add(700);
        numbers.add(25);
        numbers.add(19);

        sort(numbers);

        numbers.forEach(System.out::println);
    }

    public static void sort(List<Integer> numbers) {
        for (int i = 0; i < numbers.size(); i++) {
            String number = numbers.get(i).toString();

            StringBuilder stringBuilder = new StringBuilder();

            number = stringBuilder.append(number).reverse().toString();

            while (number.startsWith("0")) {
                number = number.substring(1);
            }

            numbers.set(i, Integer.valueOf(number));
        }

        Collections.sort(numbers);
    }
}